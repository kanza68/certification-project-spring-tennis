package com.dyma.tennis;

/* Class qui encapsule uniquement des données */
public record HealthCheck(ApplicationStatus status, String message) {

}
